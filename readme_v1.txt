###
##
## for readers of Nozue et al, 
##
###########################
"main_final.R" contains the main R scripts, which requires "function.R" for specific function in "main_final.R". 
"myCol_publication_final.R" needs to analyze RNAseq data.
"Goseq_hyper_publication.R" is used for GO ORA for RNAseq.
"RNAseqVSmicroarray_publication_final.R" is used for comparison between published microarray data and RNAseq data (this study).

Note1. User need to change working directory and directory for loading and saving files.

Note2. These scripts worked on following environment.
> sessionInfo()
R version 3.1.2 (2014-10-31)
Platform: x86_64-apple-darwin13.4.0 (64-bit)